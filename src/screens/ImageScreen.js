import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ImageDetail from '../components/ImageDetail'

const ImageScreen = () => {
    return (
        <View>
            <Text style={styles.text}>Image screen</Text>

            <ImageDetail
                title="Beach"
                imageSource={require('../../assets/beach.jpg')}
                score={1}
            />

            <ImageDetail
                title="Forest"
                imageSource={require('../../assets/forest.jpg')}
                score={7}
            />

            <ImageDetail
                title="Mountain"
                imageSource={require('../../assets/mountain.jpg')}
                score={9}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    text: {
        fontSize: 30,
        alignItems: 'center'
    }
});

export default ImageScreen;