import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

const ComponentsScreen = () => {
    const name = 'Robert';
    return (
        <View>
            <Text style={styles.bigFont}>Getting started with React Native!</Text>
            <Text style={styles.smallFont}>My name is {name}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    bigFont: {
        fontSize: 45
    },
    smallFont: {
        fontSize: 20
    }
})

export default ComponentsScreen;