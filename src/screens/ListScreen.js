import React from 'react';
import { View, FlatList, Text, StyleSheet } from 'react-native';

const ListScreen = () => {
    const friends = [
        { name: "Friend 1", age: "11" },
        { name: "Friend 2", age: "22" },
        { name: "Friend 3", age: "34" },
        { name: "Friend 4", age: "45" },
        { name: "Friend 5", age: "57" },
        { name: "Friend 6", age: "61" },
        { name: "Friend 7", age: "73" },
        { name: "Friend 8", age: "84" },
        { name: "Friend 9", age: "92" },
        { name: "Friend 10", age: "101" },
        { name: "Friend 11", age: "112" },
    ];

    return (
        <View>
            <FlatList
                keyExtractor={(friend) => friend.name}
                data={friends}
                renderItem={({item}) => {
                    return <Text style={styles.text}>Friend {item.name} - Age {item.age}</Text>
                }
                }
            />
        </View>
    );
}

const styles = StyleSheet.create({
    text: {
        marginVertical: 10,
        fontSize: 30
    }
});

export default ListScreen;