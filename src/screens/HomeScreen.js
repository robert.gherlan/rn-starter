import React from "react";
import { View, Text, Button, StyleSheet } from "react-native";

const HomeScreen = ({ navigation }) => {
  return (
    <View>
      <Text style={styles.text}>Home Screen!</Text>

      <Button
        title="Go to Components Demo"
        onPress={() => navigation.navigate("Components")}
      />

      <Button
        title="Go to Lists Demo"
        onPress={() => navigation.navigate("List")}
      />

      <Button
        title="Go to Image Demo"
        onPress={() => navigation.navigate("Image")}
      />

      <Button
        title="Go to Counter State Demo"
        onPress={() => navigation.navigate("CounterState")}
      />

      <Button
        title="Go to Counter Reducer Demo"
        onPress={() => navigation.navigate("CounterReducer")}
      />

      <Button
        title="Go to Color Demo"
        onPress={() => navigation.navigate("Color")}
      />

      <Button
        title="Go to Square State Demo"
        onPress={() => navigation.navigate("SquareState")}
      />

      <Button
        title="Go to Square Reducer Demo"
        onPress={() => navigation.navigate("SquareReducer")}
      />

      <Button
        title="Go to Text Demo"
        onPress={() => navigation.navigate("Text")}
      />

      <Button
        title="Go to Box Demo"
        onPress={() => navigation.navigate("Box")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30,
  },
});

export default HomeScreen;
